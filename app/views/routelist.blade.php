@extends('layouts.default')
@section('content')
    <div class="row">
        <div class="col-md-12">
            @include('includes.alert')
            <section class="panel">
                <header class="panel-heading">
                    {{ $title }}
                </header>

    <div class="panel-body">
        <table class="display table table-bordered table-stripe" id="list-table">
            <thead>
            <tr>
                <th>HTTP Method</th>
                <th>Route</th>
                <th class="text-center">Action</th>

            </tr>
            </thead>
            <tbody>
            @foreach($routes as $route)

                <tr>
                    <td>{{ $route->getMethods()[0] }}</td>
                    <td>{{ $route->getPath() }}</td>
                    <td class="text-center">{{ $route->getActionName() }}</td>
                </tr>

            @endforeach
            </tbody>
        </table>
    </div>

            </section>
        </div>
    </div>
@stop
@section('style')
    {{ HTML::style('assets/data-tables/DT_bootstrap.css') }}

@stop


@section('script')
    {{ HTML::script('assets/data-tables/jquery.dataTables.js') }}
    {{ HTML::script('assets/data-tables/DT_bootstrap.js') }}

    <script type="text/javascript" charset="utf-8">
        $(document).ready(function() {

            $('#list-table').dataTable({
            });
        });
    </script>
@stop